package com.employee.service;

import java.util.List;

import com.employee.dto.EmployeeDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;

public interface IEmployeeService {
	List<Employee> saveEmployee(List<EmployeeDto> employeeDto);

	List<Employee> getEmployeesOnSalary();

	List<EmployeeResponseDto> getEmployees();

	List<EmployeeResponseDto> getEmployeesAfterHike();

	List<Employee> sortByNames();

	List<Employee> sortByNamesInReverse();

	List<Employee> getDetailsByLimit();


}
