package com.employee.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.employee.dto.EmployeeDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;
import com.employee.exception.UserDefinedException;
import com.employee.repository.IEmployeeRepository;
import com.employee.service.IEmployeeService;

@Service
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private IEmployeeRepository employeeRepository;

	private static final Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);

// to add employee details into the database
	@Override
	public List<Employee> saveEmployee(List<EmployeeDto> employeeDto) {
		List<Employee> list = new ArrayList<Employee>();

		employeeDto.stream().forEach(employeeList -> {
			Employee employee = new Employee();
			if (employeeList.getAge() >= 18 && employeeList.getPhoneNo().matches("\\d{10}")
					&& employeeList.getDesignation().matches("^[a-zA-Z]*$")
					&& employeeList.getName().matches("^[a-zA-Z]*$")) {

				BeanUtils.copyProperties(employeeList, employee);
				list.add(employeeRepository.save(employee));
			} else {
				logger.info(" list not added to sql");
			}
		});
		if (!list.isEmpty()) {
			return list;
		}
		throw new UserDefinedException("Employee details not saved");

	}

//1.filter()  method based on salary greater than 50000.
	public List<Employee> getEmployeesOnSalary() throws UserDefinedException {
		List<Employee> employeeList = employeeRepository.findAll();
		List<Employee> filterList = employeeList.stream().filter(salary -> salary.getSalary() > 50000)
				.collect(Collectors.toList());
		if (!filterList.isEmpty()) {
			return filterList;
		}
		throw new UserDefinedException("Employee details not found below 50000 salary");
	}

//2.Getting employee name and designation details below 20000 salary

	public List<EmployeeResponseDto> getEmployees() {
		List<Employee> employeeList = employeeRepository.findAll();
		List<EmployeeResponseDto> response = new ArrayList<EmployeeResponseDto>();
		List<Employee> filterList = employeeList.stream().filter(salary -> salary.getSalary() < 20000)
				.collect(Collectors.toList());
		filterList.stream().forEach(list -> {
			EmployeeResponseDto employeeResponse = new EmployeeResponseDto();
			employeeResponse.setName(list.getName());
			employeeResponse.setDesignation(list.getDesignation());
			employeeResponse.setSalary(list.getSalary());
			response.add(employeeResponse);
		});
		if (!response.isEmpty()) {
			return response;
		}
		throw new UserDefinedException("Employee details not found below 20000 salary");

	}

//3.Getting employee name and designation details below 20000 salary  after giving 10000 hike
	public List<EmployeeResponseDto> getEmployeesAfterHike() {
		List<Employee> employeeList = employeeRepository.findAll();
		Employee employee = new Employee();
		List<EmployeeResponseDto> response = new ArrayList<EmployeeResponseDto>();
		employeeList.stream().filter(salary -> salary.getSalary() <= 20000).forEach(list -> {
			EmployeeResponseDto employeeResponse = new EmployeeResponseDto();
			BeanUtils.copyProperties(list, employeeResponse);
			employeeResponse.setSalary(list.getSalary() + 10000);
			response.add(employeeResponse);
			list.setSalary(list.getSalary() + 10000);
			BeanUtils.copyProperties(list, employee);
			employeeRepository.save(employee);
		});

		if (!response.isEmpty()) {
			return response;
		}
		throw new UserDefinedException("Employee details not found below 20000 salary");

	}
//4.Sort by name using sorted stream in ascending order.

	@Override
	public List<Employee> sortByNames() {
		List<Employee> employeeList = employeeRepository.findAll();
		List<Employee> sortedList = employeeList.stream().sorted(Comparator.comparing(Employee::getName))
				.collect(Collectors.toList());
		return sortedList;

	}

//5.Sort by name using sorted stream in descending order.
	@Override
	public List<Employee> sortByNamesInReverse() {
		List<Employee> employeeList = employeeRepository.findAll();
		List<Employee> sortedList = employeeList.stream().sorted(Comparator.comparing(Employee::getName).reversed())
				.collect(Collectors.toList());
		return sortedList;
	}

//6. Limit and skip the list by streams based on designation.
	@Override
	public List<Employee> getDetailsByLimit() {
		List<Employee> employeeList = employeeRepository.findAll();
		List<Employee> filteredList = employeeList.stream()
				.filter(designation -> designation.getDesignation().equalsIgnoreCase("hr"))
				.collect(Collectors.toList());
		filteredList = filteredList.stream().limit(3).skip(1).collect(Collectors.toList());
		return filteredList;
	}
}
