package com.employee.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.dto.EmployeeDto;
import com.employee.dto.EmployeeResponseDto;
import com.employee.entity.Employee;
import com.employee.service.IEmployeeService;

@RestController
@RequestMapping("employees")
public class EmployeeController {
	@Autowired
	private IEmployeeService employeeServiceImpl;

// to add employee details into the database
	@PostMapping
	public ResponseEntity<List<Employee>> saveEmployee(@RequestBody List<EmployeeDto> employeeDto) {
		List<Employee> employeeList = employeeServiceImpl.saveEmployee(employeeDto);
		return new ResponseEntity<List<Employee>>(employeeList, HttpStatus.OK);
	}

//using intermediate operations
//1.filter()  method based on salary greater than 50000.
	@GetMapping("/filter/salary")
	public ResponseEntity<List<Employee>> getEmployeesOnSalary() {

		return new ResponseEntity<List<Employee>>(employeeServiceImpl.getEmployeesOnSalary(), HttpStatus.OK);

	}

//2.Getting employee name and designation details below 20000 salary
	@GetMapping("/filter1/salary")
	public ResponseEntity<List<EmployeeResponseDto>> getEmployees() {

		return new ResponseEntity<List<EmployeeResponseDto>>(employeeServiceImpl.getEmployees(), HttpStatus.OK);

	}

//3.Getting employee name and designation details below 20000 salary  after giving 10000 hike
	@GetMapping("/filter/increaseSalary")
	public ResponseEntity<List<EmployeeResponseDto>> getEmployeesAfterHike() {

		return new ResponseEntity<List<EmployeeResponseDto>>(employeeServiceImpl.getEmployeesAfterHike(),
				HttpStatus.OK);

	}

//4.Sort by name using sorted stream in ascending order.
	@GetMapping("/sort")
	public ResponseEntity<List<Employee>> sortByNames() {

		return new ResponseEntity<List<Employee>>(employeeServiceImpl.sortByNames(), HttpStatus.OK);

	}

//5.Sort by name using sorted stream in descending order.
	@GetMapping("/reverseSort")
	public ResponseEntity<List<Employee>> sortByNamesInReverse() {

		return new ResponseEntity<List<Employee>>(employeeServiceImpl.sortByNamesInReverse(), HttpStatus.OK);

	}

//6. Limit and skip the list by streams based on designation.
	@GetMapping("/limit")
	public ResponseEntity<List<Employee>> limtByDesignation() {

		return new ResponseEntity<List<Employee>>(employeeServiceImpl.getDetailsByLimit(), HttpStatus.OK);

	}

}
