package com.employee.exception;


public class UserDefinedException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String msg;

	public UserDefinedException(String msg) {
		this.msg = msg;
	}

	public String getMessage() {
		return msg;
	}

}
